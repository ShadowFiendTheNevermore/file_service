<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('files')->group(function () {

    Route::middleware('auth:api')->group(function () {
        // get private
        Route::prefix('private')->group(function () {
            Route::get('/{file}', 'FilesController@getPrivate');
        });

        // upload file
        Route::post('/upload', 'FilesController@upload');
    });

    Route::get('/public/{file}', 'FilesController@getPublic');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
