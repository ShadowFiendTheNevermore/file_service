<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\FileUpload;
use App\Models\FileUploads;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UploadFileRequest;

class FilesController extends Controller
{

    public function upload(UploadFileRequest $request)
    {
        $file = $request->file('file');
        $disk = 'private';

        // Можно сделать автозаполнение полей для модели
        $upload = new FileUpload;
        $upload->user_id = $request->user()->id;
        $upload->original_filename = $file->getClientOriginalName();
        $upload->mime_type = $file->getMimeType();

        // Проверяем на флаг временного хранения
        if ($request->query('is_temporary', true)) {
            $upload->temporary_for = Carbon::now()->addDays(config('upload.temp_days'));
        }

        // Проверяем на флаг приватного хранения и храним в разных каталогах
        $disk = $request->query('is_private', false) ? 'private' : 'public';
        $upload->is_private = $request->query('is_private', false);
        $upload->filename = $file->store('', ['disk' => $disk]);

        $upload->save();
        $path = Storage::disk($disk)->url($upload->filename);

        return $path;
    }

    /**
     * Get private images from storage
     *
     * @param string $filename
     * @param Request $request
     * @return Response
     */
    public function getPrivate($filename, Request $request)
    {
        $disk = Storage::disk('private');

        if (!$disk->exists($filename)) {
            abort(404, "File doesn't exists");
        }

        $upload = FileUpload::firstWhere('filename', $filename);

        if (!$upload) {
            abort(404, 'Missing data for file');
        }

        if (Carbon::now() > $upload->temporary_for && $upload->temporary_for !== null) {
            abort(410, 'Temporary limit is expired');
        }

        return $request->query('download', false) ? $disk->download($filename)
            : response()->file($disk->path($filename));
    }

    /**
     * Get public files
     *
     * @param string $filename
     * @return Response
     */
    public function getPublic($filename, Request $request)
    {
        $disk = Storage::disk('public');

        // DRY - можно вынести в middleware
        if (!$disk->exists($filename)) {
            abort(404);
        }

        // DRY
        $upload = FileUpload::firstWhere('filename', $filename);

        // DRY
        if (!$upload) {
            abort(404, 'Missing data for file');
        }

        // DRY - вынести в middleware
        if (Carbon::now() > $upload->temporary_for && $upload->temporary_for !== null) {
            abort(410, 'Temporary limit is expired');
        }

        // DRY
        return $request->query('download', false) ? $disk->download($filename)
            : response()->file($disk->path($filename));
    }
}
